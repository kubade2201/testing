#5986452 03/21/23 AT 1:43 PM TEST CYCLE #406425
macOS Monterey 12.6.3 - Gift Card Scam Awareness - failed to open the page.
  
TESTER
Jakub Nabywaniec
 
STATUS
Approved
 
VALUE
somewhat valuable
 
BUILD
5
 
MODIFIED
03/21/23 at 5:43 PM
 
SEVERITY
Critical
 
ISSUE TYPE
Crash
 
SOURCE
Structured
 
FREQUENCY
Every Time
ENVIRONMENT
  macOS Monterey 12.6.3 - Safari
DESCRIPTION
MESSAGES
DUPLICATE BUG WARNING
 Do not report duplicates
Make sure your issue was not already reported - these 6 issues look similar:

Highlight Duplicate Wording

1 macOS Monterey 12.6.3 - Birthday Gift - The product image missing  
1 macOS Monterey 12.6.3 - PDP - Full video not uploaded for viewing  
1 macOS Monterey 12.6.3 - Beauty on Demand - Active empty layout section  
1 Windows 11 - Cologne Gift Sets - The Versace mini cologne gift set displays product unavailable page  
1 macOS Ventura - PLP - Some product images are missing.  
COMMUNITY REPRODUCTIONS
Show 3 Reproductions
  
ACTION PERFORMED
1. Open https://www.sephora.com

2. Scroll the page down.
3. Open "Gifts Cards"
4. Open "Gift Card Scam Awareness"
5.Open link from "Prevention Tips"
EXPECTED RESULT
The page are opened without problems.
ACTUAL RESULT
Failed to open page
ERROR MESSAGE
Safari can't find the server
EXACT URL  
https://ftccomplaintassistant.gov
DID YOU ATTACH AN MP4 VIDEO?  
Yes, I did upload an mp4 video
DID YOU ATTACH A SCREENSHOT?  
Yes, I did upload a highlighted screenshot of the issue.
DID YOU ATTACH A LOG WITH TIMESTAMP? 
Yes, I did upload a device log or console log in .txt format

[photo](https://utest-dl.s3.amazonaws.com/12279/20932/406425/5986452/bugAttachment/Bug5986452_Screenshot_2023-03-21_at_13.22.37.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T190434Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=3c56c37a6ad981f01221ceb1a9b70d320985973d7669a20f1ac6076999246ca1)
[video](https://utest-dl.s3.amazonaws.com/12279/20932/406425/5986452/bugAttachment/Bug5986452_Screen_Recording_2023-03-21_at_13.31.00-1.mp4?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T190525Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=57628b4b1a8e7689442cf601e8c831641fa501d032433ff01a10e540e00fcfa6)
[logs](https://utest-dl.s3.amazonaws.com/12279/20932/406425/5986452/bugAttachment/Bug5986452_logi.txt?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T190557Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=c14061881c016fa68e44d669052c8cf85c65cae752a69c6c8843a2518b1ea23b)
