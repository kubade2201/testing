Claim this test case and finish all the steps to complete this Academy test cycle. Make sure to submit the test case before the cycle locks.
SLOT DESCRIPTION
Computer - Any browser

By claiming this slot you will test in this cycle using a Computer device on any browser.

COMMENTS

Approved by: Manav Nai

## Step 1 - About Screen
## STEP INSTRUCTIONS
Upload a screenshot of your device's about screen showing the exact OS/edition used to test

Hint:
• Windows: Press the Windows key + R on the keyboard > Type winver in the text field and press Enter
• macOS: Choose Apple menu > About This Mac
• Android: Settings > About phone > Software information or Android version
• iOS: Settings > General > About
RESULT ATTACHMENTSMINIMUM 1
A screenshot of device's about screen showing exact OS/edition has been uploaded.
[Photo](https://utest-dl.s3.amazonaws.com/testRunStepMinAttachmentsResult/30149715/Screenshot_2023-03-20_at_12.18.18.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T185323Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=712d5257a03ad5734fc7d6300908fe64630ee580d72313df6e786208fa819942)

## Step 2 - Registration and Preference
## STEP INSTRUCTIONS
1. Open the testing website
2. Create an account with valid credentials
3. Test the registration, login/out
4. Test the available account preferences

Notes:
• Select "Pass" if you are able to create an account, able to log in/out to the account successfully, and the available account preferences work as expected

• Click on "Fail" then select "Fail & Report issue" if you are not able to create an account and log in/out successfully. Then, you need to delete the automatically filled actions performed, expected and actual result fields and start to write the report by following the bug report instructions in the overview (Make sure to check if no one reported this issue before to avoid rejection as duplicate)

• Click on "Fail", if you are not able to create an account and log in/out successfully and another tester has already reported the same issue (Make sure to write the already reported issue ID into the actual result field after you select "Fail" of this step and confirm (+1) the original report)
EXPECTED RESULTS
1. The Homepage is opened
2. The account was created as expected
3. Logging in and out of the account without any problems
4. The available account preferences work as expected
STATUS
COMMENT
-
RESULT ATTACHMENTSMINIMUM 1
At least 1 screenshot showing your account has been uploaded to this step.
[Photo](https://utest-dl.s3.amazonaws.com/testRunStepMinAttachmentsResult/30149717/Screenshot_2023-03-20_at_12.24.14.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T185450Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=73ee2d26caaca7bc2f088baaaf888283c278e1786dabcbe33bcd87a0903fd931)

## Step 3 - Search
## STEP INSTRUCTIONS
1. Click on the search field and use valid keywords
2. Open any suggested result and ensure that it is redirecting to the proper result page and ensure the results render correctly
3. Test all the available features on the search results page and ensure the search function works as expected

Notes:
• Select "Pass" if all features on the search page work as expected

• Click on "Fail" then select "Fail & Report issue" if you found an issue in one of those features on the search page. Then, you need to delete the automatically filled actions performed, expected, and actual result fields and start to write the report by following the bug report instructions in the overview (Make sure to check if no one reported this issue before to avoid rejection as duplicate)

• Select "Fail" if you found an issue in one of those features on the search page, but another tester has already reported the same issue (Make sure to write the already reported issue ID into the actual result field after you select "Fail" of this step and confirm (+1) the original report)
EXPECTED RESULTS
1. The search feature is accessible
2. Suggestions direct to the correct results page
3. All available features on the search page work as expected

[Photo](https://utest-dl.s3.amazonaws.com/testRunStepMinAttachmentsResult/30149719/Screenshot_2023-03-20_at_12.26.30.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T185544Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=8557ae5593d5cb4555eb3fea5837f9e97711c1c241fa82c705ad40dc0659024d)

## Step 4 - Product Listing Page
## STEP INSTRUCTIONS
1. Open any category and a subcategory (e.g. Skincare > Treatments > Face Serums etc.)
2. Test the functionality of the available features on the product listing page, such as Filter, Sort, etc.

Notes:
• Select "Pass" if all features on the product listing page work as expected

• Click on "Fail" then select "Fail & Report issue" if you found an issue in one of those features on the product listing page. Then, you need to delete the automatically filled actions performed, expected, and actual result fields and start to write the report by following the bug report instructions in the overview (Make sure to check if no one reported this issue before to avoid rejection as duplicate)

• Select "Fail" if you found an issue in one of those features on the product listing page, but another tester has already reported the same issue (Make sure to write the already reported issue ID into the actual result field after you select "Fail" of this step and confirm (+1) the original report)
EXPECTED RESULTS
1. All category pages render properly when opened
2. Filter options work as expected
2. Sorting options work as expected
[Photo](https://utest-dl.s3.amazonaws.com/testRunStepMinAttachmentsResult/30149721/Screenshot_2023-03-20_at_12.36.04.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T185641Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=83dbebbf07fc221fd023a07040d44f1d775dd3a757930bed4907ab7cbebeec33)

## Step 5 - Product Details Page
## STEP INSTRUCTIONS
1. Open a product detail page for any category
2. Test the functionality of all the available features on the product details page, such as About the Product, Highlights, Reviews, Add to Loves, Change product quantity and size, etc.

Notes:
• Select "Pass" if all features on the product details page work as expected

• Click on "Fail" then select "Fail & Report issue" if you found an issue in one of those features on the product details page. Then, you need to delete the automatically filled actions performed, expected, and actual result fields and start to write the report by following the bug report instructions in the overview (Make sure to check if no one reported this issue before to avoid rejection as duplicate)

• Select "Fail" if you found an issue in one of those features on the product details page, but another tester has already reported the same issue (Make sure to write the already reported issue ID into the actual result field after you select "Fail" of this step and confirm (+1) the original report)
EXPECTED RESULTS
1. Product detail pages render properly when opened
2. The product details display correct and related information about the product
2. Product images are rendered correctly when navigating through them
2. The review features work as expected
2. Changing the product size on the product details page work as expected
2. Add to Loves from the product details page work as expected
2. Changing the product quantity work as expected

[Photo](https://utest-dl.s3.amazonaws.com/testRunStepMinAttachmentsResult/30149723/Screenshot_2023-03-21_at_13.07.29.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T185721Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=2fb1cbbe839f69f96fa9141de09eaaf5f5c97d171de0d3fe0d4fbd653cd47a0e)

## Step 6 - Basket
## STEP INSTRUCTIONS
(Please DO NOT complete placing any order. No reimbursement will be done)

1. Open the product details page from any category
2. Select any available options for the product, such as color, size, etc.
3. Add the product to the basket
4. Open the basket page and ensure the price is calculated correctly
5. Move products to Loves and then add them back to the basket and ensure the basket is updated correctly
6. Update the quantity on the bag and ensure the price is calculated correctly
7. Change delivery method
8. Remove any or all of the product(s) from the bag

Notes:
• Select "Pass" if you are able to add and remove products from the bag successfully

• Click on "Fail" then select "Fail & Report issue" if you found an issue when you add or remove the products from the bag page. Then, you need to delete the automatically filled actions performed, expected, and actual result fields and start to write the report by following the issue report instructions in the overview. (Make sure to check if no one reported this issue before to avoid rejection as duplicate)

• Select "Fail" if you found an issue when you add or remove the products from the bag page, but another tester has already reported the same issue (Make sure to write the already reported issue ID into the actual result field after you select "Fail" of this step and confirm (+1) the original report)
EXPECTED RESULTS
3. Adding the product to the basket works as expected
4. In the basket, you can view the products you have added, and the price is calculated correctly
5. The basket is correctly updated after moving some products to Loves
6. The quantity is updated correctly and the price is calculated as expected
7. Delivery method is changed and the basket page is updated correctly
8. The product(s) can be removed from the bag as expected
[Photo](https://utest-dl.s3.amazonaws.com/testRunStepMinAttachmentsResult/30149725/Screenshot_2023-03-20_at_13.04.08.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T185810Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=f1a8227d46a8c8295caad484c082f4489d59d9609517b6a3bc7eebd863c0766e)

## Step 7 - Submitting a Review
## STEP INSTRUCTIONS
1. Review this course to learn how to write a review: https://www.utest.com/academy/tracks/33/courses/tester-review
2. Click on the "Write a Review" button on this test cycle
3. Answer the questions based on your experience testing and learning in the Academy testing cycles
4. Open the attached screenshot in this step to see how a quality review looks

• Tip: Answer the review questions inside the uTest Platform and click submit (if you do not click submit, the answers to the review questions will not be saved)

• Select "Done" in this step after you have answered the review questions and clicked submit. Follow the instructions closely

• Click "Submit Results" button, and specify the spent time and then click "Finish" button

[Photo](https://utest-dl.s3.amazonaws.com/12279/testCaseStep/10291602/1/Review-_Example.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20230412T185856Z&X-Amz-SignedHeaders=host&X-Amz-Expires=86400&X-Amz-Credential=AKIAJ2UIWMJ2OMC3UCQQ%2F20230412%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=163d16e7e0a35d966bebe60d92305659a88f14aa89efe8a9d8cba83aa78fe91c)
